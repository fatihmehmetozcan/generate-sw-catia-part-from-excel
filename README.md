I prepared a small macro in Excel to generate a part using cell values as dimensions. This is very basic but after you adjust it to your needs will become very handy.


Prompts user to choose a path to save as or goes on without saving. With entered values in specific cells, a part is generated from it's template.


Using a template part is more useful than creating a new one every time. You can use every kind of part you have as template when you change dimensions in code.


My file is in 2018, to use this macro in another version just create a part with the same dimension names as in code.


Go to https://forum.solidworks.com/message/933566 for any comments about this project.


**Preconditions:**
- SolidWorks working in background
- SolidWorks type library reference added
- Template part and excel file needs to be in same folder
- If template part and excel file name need to be changed, specify in const variables for macro to work as expected

![0](/Capture-XL.PNG)